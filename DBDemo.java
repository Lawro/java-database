import java.util.*;
import java.io.*;
import javax.swing.*;

public class DBDemo {
   private Database database;
   private Table table;
   private Record record;
   private JFrame f;

   public static void main(String args[]) {

      JFrame f = new JFrame();

      Database database = new Database("DB");

      // Create a new table for pet owners, add to DB
      Table petowner = new Table();
      String[] powner = {"Name", "Surname", "Pet"};
      database.createTable("Pet Owners", powner);

      // Populate row(s) with owner/pet data
      petowner = database.getTable("Pet Owners");
      petowner.insert("1");
      petowner.update("1", 0, "Peter");
      petowner.update("1", 1, "Smith");
      petowner.update("1", 2, "Cat");

      petowner.insert("2");
      petowner.update("2", 0, "Jane");
      petowner.update("2", 1, "Johnson");
      petowner.update("2", 2, "Dog");

      Record arec = petowner.select("1");
      String str = String.join(",", arec.getArray());
      String[][] data = petowner.recArr();

      JTable jt = new JTable(data, powner);

      jt.setBounds(30,40,200,300);          
      JScrollPane sp=new JScrollPane(jt);    
      f.add(sp);          
      f.setSize(300,400);    
      f.setVisible(true);

      // Create a new table for sports teams, add to DB
      Table sportsteam = new Table();
      String[] steam = {"Team", "Sport"};
      database.createTable("Sports Teams", steam);

      // Populate row(s) with team/sport data
      sportsteam = database.getTable("Sports Teams");
      sportsteam.insert("A");
      sportsteam.update("A", 0, "Crystal Palace");
      sportsteam.update("A", 1, "Football");

      sportsteam.insert("B");
      sportsteam.update("B", 0, "Lions");
      sportsteam.update("B", 1, "Rugby");

      // Create an empty table for food types, add to DB
      Table foodtype = new Table();
      database.addTable("Food Types", foodtype);

      foodtype = database.getTable("Food Types");
      // Added new columns
      foodtype.addCol(0, "Food");
      foodtype.addCol(1, "Type");
      // Added a Mistake by 'mistake'
      foodtype.addCol(2, "Mistake");
      // Deleted a Mistake on purpose
      foodtype.delCol(2);

      // Populated row(s) with food/type data
      foodtype.insert("1A");
      foodtype.update("1A", 0, "Apples");
      foodtype.update("1A", 1, "Fruit");

      foodtype.insert("2B");
      foodtype.update("2B", 0, "Beef");
      foodtype.update("2B", 1, "Meat");

      foodtype.insert("3C");
      foodtype.update("3C", 0, "Milk");
      foodtype.update("3C", 1, "Dairy");
      

      /* Prints out all tables stored in Databese.
         Probably not ideal for a large database! */
      database.displayDB();
      
      // Deletes the Sports Teams column
      database.delTable("Sports Teams");
      database.displayDB();

      // Saves the tables to files in folders
      database.dbSaveTbls();

      /* Removes all tables from the database, ready
         for it to be closed */
      database.dbClose();

      /* Reloads the tables to the database from 
         files saved previously */
      database.dbLoadTbls();
      database.displayDB();
   }
}