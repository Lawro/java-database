import java.util.*;
import java.io.*;

//Class to store String data in separate cells
public class Record implements java.io.Serializable
{
	private ArrayList<String> record;

   // Record constructor
	Record () {
		record = new ArrayList<String>();
	}

   // Get number of cells
   int size () {
      return record.size();
   }

   // Gets an array from the Array<List> data
   String[] getArray() {
		if (record.size() > 0){
      	String[] arr = record.toArray(new String[0]);
   		return arr;
   	}
		else {
			System.err.println("Empty table, no data to fetch!");
			String[] arr = {};
			return arr;
		}
   }

   // Add a String array
	void addArr (String[] data) {
		for(int i = 0; i < data.length; i++){
			record.add(data[i]);
		}
	}

   // Overwite an existing cell
	void setElem (int col, String att) {
		if (col <= record.size()){
			record.set(col, att);
		}
		else {
			System.err.println("Cannot overwrite a non-existent column!");
		}
	}

   // Add a new cell
	void addElem (int col, String att) {
		record.add(col, att);
	}

   // Delete a cell
	void delElem (int col) {
		if (col <= record.size()){
			record.remove(col);
		}
		else {
			System.err.println("Cannot delete a non-existent column!");
		}
	}

   // Padding for printing
	public static String padRight (String s, int n) {
     return String.format("%1$-" + n + "s", s);  
	}

   // Display and format the Record
	void displayRec () {
		for (int i=0; i<record.size(); i++){
			String rec = record.get(i);
			System.out.print("| ");
			System.out.print(padRight(rec, 15));
		}
		System.out.print(" |");
		System.out.println("");
		for (int j=0; j<record.size(); j++){
			String len = record.get(j);
			for (int k=0; k<18; k++)
				System.out.print("_");
		}
		System.out.println("");
	}

}