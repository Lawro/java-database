import java.io.*;
public class FileHandler {

	private Table table;
	private Database database;

   /* save a table to a file */
	void FileSave (Table s) {

		try {
			System.out.println(s.getName());
	   	File yourFile = new File(s.getName());
			boolean result = yourFile.mkdir();
			FileOutputStream fileOut =
			new FileOutputStream("./" + s.getName() + "/" + s.getName() + ".ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(s);
			out.close();
			fileOut.close();
			System.out.println("Serialized data is saved in fname.ser");
		}catch(IOException i) {
			i.printStackTrace();
		}
	}

   /* load a table from a file */
	Table FileLoad (String fname) {

		Table l = null;
		try {
			FileInputStream fileIn = new FileInputStream(fname);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			l = (Table) in.readObject();
			in.close();
			fileIn.close();
			System.out.println("Serialized data is loaded from fname.ser");
			return l;
		}catch(IOException i) {
			i.printStackTrace();
			return l;
		}catch(ClassNotFoundException c) {
			System.out.println("Table class not found.");
			c.printStackTrace();
			return l;
		}
	}

}