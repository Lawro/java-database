Java Database

----------------------------------------------------------------------------------------------------
DBDemo.java

This class serves as a demo driver of the database, and creates a Database and some Tables, 
populates them, saves them, loads them, and displays them (not necessarily in that order).


----------------------------------------------------------------------------------------------------
Record.java

Initially the Record class utilised a String array to store its data. However, upon implementing 
the Table class, it became apparent that the immutable nature of arrays once initialised meant that 
using the Table's 'alter' method would mean having to create a new array for each Record, copying 
data from the old one to a resized array, and then overwriting the old array.

Considering the Table class utilises an ArrayList (see Table.java below) to allow dynamic alteration 
of array data, it made sense to update the Record class to do the same. This meant for an 
additional print method within the Record class to handle the printing of ArrayList data, much the 
same as in the Table class.

Each Record is constructed with a String array, so that it is filled with data from the start. It 
was decided that the number of elements had to much the number of the current Table's attributes, 
both to make display/printing easier but to ensure there couldn't be fewer columns than row data.


----------------------------------------------------------------------------------------------------
Table.java

An ArrayList data type was utilised in the Table class to allow dynamic insertion, deletion, and 
alteration of data, rather than immutable arrays.

Inheritance was attempted by extending from the Record class, but initially abandoned after 
problems with the Table constructor unable to inherit a non-existent default, no-param constructor 
from Record. Both the Record and Table constructors created new instances and simulatenously added
attribute data. Efficient at first, but even using super(params) seemed to fail for inheritance.

Of course, this meant the classes became rather WET ("With Excessive Text" aka un-DRY), and so 
constructor classes for Record and Table were refactored to simply initialise a new Record/Table,
pushing the likes of setting a name, attributes, keys etc. to additional methods. This refactoring
also meant the Database class could gain inheritance from the Table class, saving on yet more
duplicate code.

The 'insert' and 'delete' methods were initially implemented to tackle the very end of the 
ArrayList, just to get them working. Indexing was added later to allow for adding/removing rows 
where required, and a similar approach was applied to the 'alter' method to allow addition of 
columns at any required index. Given more time, this could be expanded to remove an entire column 
and its data.


----------------------------------------------------------------------------------------------------
FileHandler.java

Different methods of file handling were looked into to tackle this problem, with many outlined 
here: http://www.baeldung.com/java-write-to-file.

However, the method that wasn't present in the above is Serialization (albeit a similar one is, 
namely FileOutputStream) is serialization. Code in FileHandler.java was adapted from: 
https://www.tutorialspoint.com/java/java_serialization.htm.

Each class that depends on serialization has to use "implements java.io.Serializable" in its 
declaration.

This seemed like a good, modern Java approach, that saved the Table data to a binary file, and 
efficiently loaded it straight back into a Table object without having to loop through elements to 
save them into new Records.

A potential downside to writing to binary, however, means that the author, or indeed anyone else, 
cannot reasonably open the save data to edit in, say, a text editor. Perhaps using FileChannel and 
saving out individual row Record string arrays would be better, but one could argue for code 
efficiency and also security.

File handling was moved to the Database wrapper, so that it could save out all tables to 
appropriate folders and filenames based on their Table key names. Unfortunately serialization of 
the database itself proved too difficult for now, but to illustrate the loading, a database could
be cleared of all tables using .dbClose(), and then reloading the tables using dbLoadTbls(). A new
database could also be created with a copy of the previous database's Table names, which are used
as a reference to load the Tables. Alternatively, another approach could be to save the table names
to a separate file, and use that as a basis for a new database to reference Table names from which
to load.


----------------------------------------------------------------------------------------------------
Printing

Display handling is pretty simple and built into both the Table and Record classes to handle their 
nuances separately. A padding of 10 spaces was used purely for readability and to ensure each row 
element lined up, though this could be altered to any length in case longer strings are used.

Line divider lengths aren't quite correct as they are hard-coded to a current max cell length of 18
characters, although this could be changed to reflect any theoretical maximum cell length.


----------------------------------------------------------------------------------------------------
Keys

An initial approach was to add on to the existing Table and/or Record classes as required, for 
example by inserting a new column at index 0, calling it "Keys", and then generating a new, unique 
key for each table entry. While this might be easy in terms of utilising existing code, the danger 
could lie in ensuring unique keys, with lengthy checks.

Thus it was decided to use a HashMap, allowing the user to use their own key (checked with the 
.putIfAbsent method from the Map). This meant a minor modification to the Table class, changing its
type from ArrayList<Record> to HashMap<String, Record> and changing all .add methods to .put. To 
ensure unique keys, .putIfAbsent was used, throwing up an error if the key was already taken.

The drawback here is that hashmaps cannot guarantee their order (not a big problem considering the
keys are the import aspect), and they display in reverse order to what might be expected (this is 
a problem in this case).

Instead a LinkedHashMap was used, and thus order was maintained both in insertion and display.


----------------------------------------------------------------------------------------------------
Database.java

It was decided that a Database class would be implemented to wrap up tables. It purely handles
creation and storage of tables in another LinkedHashMap, in order to store a key for each table to 
use as a name and to ensure duplicate entries couldn't be added via putIfAbsent(). Saving of Tables 
was moved to the Database class from the Table class, in order to save out all Tables present 
within the Database.


----------------------------------------------------------------------------------------------------
Extension

For an extension, a GUI display of tables was chosen. Specifically, the JTable class from the 
'swing' GUI library was used, with help from: https://www.javatpoint.com/java-jtable.

The first table in the DBDemo class is displayed using JTable. This is not fully implemented and 
would ultimately be built into the Database class, but rather indicates a proof of concept that a 
table can be displayed using JTable. This could be expanded upon greatly to implement a full-blown 
tabbed spreadsheet of the database, given (much) more time.

The trick here was to get the data back out of the Records and Tables, which required additional
array-getting methods, which converted the ArrayLists of each Record into String arrays using the
toString() method, accessed via the keys from the Table via the keySet() method.

JTables take in 2D arrays to display the tables, as expected, and so each Record was added to a new
row of the 2D String[][] array, to be displayed in the JTable.
