import java.util.*;
import java.io.*;

// Class to store cell data from a Record in form of a Table
public class Table extends Record implements java.io.Serializable {
	private LinkedHashMap<String, Record> table;
   private Record record;
   private String name;

   /* Table constructor, adds empty first row for column attributes */
	Table () {
      table = new LinkedHashMap<String, Record>();
	}

   // Returns string array from all records in Table
   String[][] recArr () {
      String[] keys = table.keySet().toArray(new String[0]);
      String[][] str = new String[table.size()][table.get("Key").size()];
      for (int i=0; i<table.size(); i++){
         String[] arr = table.get(keys[i]).getArray();
         for (int j=0; j<table.keySet().size(); j++){
            str[i][j] = arr[j];
         }
      }
      return str;
   }

   // Sets the first key value to "Key"
   void setKey () {
      record = new Record();
      table.put("Key", record);
   }

   // Returns the set of keys the Table stores
   Set<String> getKeySet () {
      return table.keySet();
   }

   // Sets the name of the Table
   void setName (String tname) {
      name = tname;
   }

   // Gets the name of the Table
   String getName () {
      return name;
   }

   /* select a row */
   Record select (String key) {
      return table.get(key);
   }

   /* insert a new, empty row with user-defined key */
	void insert (String key) {
      record = new Record();
      for (int i=0; i<table.get("Key").size(); i++){
         record.addElem(i, "");
      }
		table.putIfAbsent(key, record);
	}

   /* delete a row */
	void delete (int n) {
      if (n > 0 && table.size() > 1){
         table.remove(n);
      }
      else {
         System.err.println("Cannot remove column names from non-empty table.");
      }
	}

   /* update record */
   void update (String key, int col, String att) {
      Record newatt = table.get(key);
      newatt.setElem(col, att);
   }

   // Adds an array of attributes to a row
   void addAtts(String[] atts) {
      record = table.get("Key");
      record.addArr(atts);
   }

   // Adds an empty column
   void addCol (int col, String newatt) {
      for (Record value : table.values()) {
         value.addElem(col, "");
      }
      Record record = table.get("Key");
      record.setElem(col, newatt);
      table.put("Key", record);
   }

   // Deletes an entire column
   void delCol (int col) {
      if (table.size() > 0){
         for (Record value : table.values()) {
            value.delElem(col);
         }
      }
      else {
         System.err.println("Cannot have a table with only keys and no data.");
      }
   }

   // Displays the Table and its rows
   void display () {
      System.out.println("Table: " + name);
      System.out.println("");
      table.forEach((k, v) -> {
         v.displayRec();
         System.out.println();
      });
      System.out.println("");
   }   

   // Returns the size of the Table
   int size() {
      return table.size();
   }

}