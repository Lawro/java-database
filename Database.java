import java.util.*;
import java.io.*;

/* Controls the game */
public class Database extends Table implements java.io.Serializable {
   private Table table;
   private LinkedHashMap<String, Table> database;
   private FileHandler file;
   private String name;
   private ArrayList<String> tblnames;

   // Database constructor, adds db name and a table list
   Database (String dname) {
      database = new LinkedHashMap<String, Table>();
      name = dname;
      tblnames = new ArrayList<String>();
   }

   // Creates a new table in the database
   void createTable (String tname, String[] atts) {
   	table = new Table();
      table.setName(tname);
      table.setKey();
      table.addAtts(atts);
   	database.putIfAbsent(tname, table);
   	tblnames.add(tname);
   }

   // Returns the table names
   ArrayList<String> getTblNames(){
      return tblnames;
   }

   // Initialise and add empty table with "Key"
   void addTable (String tname, Table table){
   	table.setKey();
   	table.setName(tname);
   	database.putIfAbsent(tname, table);
   	tblnames.add(tname);

   }

   // Returns a specific table based on a key
   Table getTable (String key){
   	return database.get(key);
   }

   // Deletes a table based on a key
   void delTable (String key){
   	database.remove(key);
   	tblnames.remove(key);
   }

   // Returns the number of tables in the database
   int size () {
      return database.size();
   }

   // Displays the tables in the database
   void displayDB () {
      database.forEach((k, v) -> {
         v.display();
      });
   }

   // Saves the Tables in the database to folders
   void dbSaveTbls (){
   	file = new FileHandler();
      database.forEach((k, v) -> {
   		file.FileSave(v);;
      });
   }

   // Loads the Tables into the database from files
   void dbLoadTbls (){
   	file = new FileHandler();
   	for (int i=0; i<tblnames.size(); i++){
   		Table load = new Table();
   		load = file.FileLoad("./" + tblnames.get(i) + "/" + tblnames.get(i) + ".ser");
   		database.put(tblnames.get(i), load);
   	}
   }

   // Unloads the Tables from the database, ready for closing
   void dbClose (){
   	for (int i=0; i<database.size(); i++){
   		database.remove(i);
   	}
   }

}